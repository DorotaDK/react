import React, { Component } from 'react';
import './App.css';
import ValidationComponent from './ValidationComponent/ValidationComponent';
import CharComponent from './CharComponent/CharComponent';

class App extends Component {

  constructor() {
    super();
      this.state = {
        textValue : ''
      }
  }

  getValueFunction = (event) => {
    this.setState({textValue: event.target.value})
   }
  
  deleteCharHandler = (event, index) => {
    const charArray = this.state.textValue.split('');
    charArray.splice(index, 1);
    let changedText = charArray.join('');
    this.setState({textValue: changedText})
  }

  render() {
    
    const charArray = this.state.textValue.split('');

    return (
      <div className="App">
        <input 
          type="text"
          value = {this.state.textValue}
          onChange = { (event) => this.getValueFunction(event) }
        />

        <ValidationComponent
          length = {this.state.textValue.length}
        /> 

        <div>
          {charArray.map((char, index) => { 
            return <CharComponent 
            click={() => this.deleteCharHandler(index)}
            key = {index}
            char = {char}
            />
          })}
        </div>
    </div>
    );
  }
}

export default App;
