import React from 'react';
// import './ValidationComponent.css'; 

const ValidationComponent = (props) => {

    let dispalyedText = props.length <= 5 ? 'Text is too short!'
                                          : 'Text long enough :)';
    return (
        <div className="ValidationComponent">
            <p> Text length: {props.length} {dispalyedText}</p>
        </div> 
    ) 
}

export default ValidationComponent;